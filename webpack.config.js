const path = require('path')
const webpack = require('webpack')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

let config = {
  devtool: false,
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'http://localhost:8080/dist/',
    filename: 'main.js'
  },
  resolve: {
    extensions: ['.js', '.ts', '.vue']
  },
  devServer: {
    noInfo: true,
    overlay: true
  },
  module: {
    rules: [{
      test: /\.ts$/,
      use: [{
        loader: 'ts-vue-loader'
      }, {
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          appendTsSuffixTo: [/\.vue$/]
        }
      }]
    },
      {test: /\.html$/, use: 'vue-template-loader'}
    ]
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    })
  ]
}

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin())
} else {
  config.devtool = 'cheap-module-eval-source-map'
  config.plugins.push(new webpack.NamedModulesPlugin())
}

module.exports = config
