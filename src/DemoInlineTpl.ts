import {Vue, Component, Prop} from 'vue-property-decorator'

@Component({
  template: `<div>
    <h1>Template en ligne</h1>
    <p>
      Je suis un composant avec le template en chaine de caractères, mon state n'est pas conservé en cas de modifications,
      mais je supporte le hot reload.
    </p>
    <p>
      Compteur (props) : {{ time }}<br>
      Compteur : {{ start + number }}
    </p>
    </div>
  `
})
export default class DemoInlineTpl extends Vue {
  @Prop({type: Number, default: 0}) start: number
  @Prop({type: Number, default: 0}) time: string
  number: number = 0
  timer: number|null

  mounted () {
    this.timer = window.setInterval(() => {
      this.number++
    }, 1000)
  }

  destroyed () {
    if (this.timer) {
      window.clearInterval(this.timer)
    }
  }
}
