import {Vue, Component, Prop} from 'vue-property-decorator'
import Render from './App.html'

@Render
@Component({})
export default class Demo extends Vue {
  @Prop({type: Number, default: 0}) start: number
  number: number = 0
  timer: number|null

  get number_formatted (): string {
    return new Intl.NumberFormat().format(this.number + this.start)
  }

  mounted () {
    this.timer = window.setInterval(() => {
      this.number++
    }, 1000)
  }

  destroyed () {
    if (this.timer) {
      window.clearInterval(this.timer)
    }
  }
}
