import {Vue, Component} from 'vue-property-decorator'
import Demo from './Demo'
import DemoInlineTpl from './DemoInlineTpl'

@Component({
  components: {Demo, DemoInlineTpl},
  template: `<div><Demo></Demo><DemoInlineTpl :time="time"></DemoInlineTpl></div>`
})
export default class App extends Vue {
  time: number = 0
  timer: number|null

  mounted () {
    this.timer = window.setInterval(() => {
      this.time++
    }, 1000)
  }

  destroyed () {
    if (this.timer) {
      window.clearInterval(this.timer)
    }
  }

}
